export const environment = {
  production: true,
  api_url: 'https://fjkzbn2rgh.execute-api.ap-southeast-1.amazonaws.com/api/tasks',
  friends_api_url: 'https://fjkzbn2rgh.execute-api.ap-southeast-1.amazonaws.com/friends',
  artists_api_url: 'https://fjkzbn2rgh.execute-api.ap-southeast-1.amazonaws.com/artists'
};
